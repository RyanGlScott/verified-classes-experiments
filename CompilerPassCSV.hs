#!/usr/bin/env stack
-- stack --resolver lts-13.7 script --optimize
{-# LANGUAGE NamedFieldPuns #-}
{-# OPTIONS_GHC -Wall #-}
module Main (main) where

import Data.Foldable
import Data.Function
import Data.List.Extra
import qualified Data.List.NonEmpty as NE
import Data.List.NonEmpty (NonEmpty)
import qualified Data.Text as T
import qualified Data.Text.IO as T
import Options.Applicative
import Safe
import Text.Printf

newtype Args = Args
  { input :: FilePath
  } deriving (Eq, Ord, Read, Show)

argParser :: Parser Args
argParser = Args
  <$> strOption
      (  long "input"
      <> short 'i'
      <> metavar "FILE"
      <> help "Where to read the input" )

main :: IO ()
main = execParser opts >>= drive
  where
    opts = info (argParser <**> helper)
      ( fullDesc
     <> progDesc spiel
     <> header spiel )

    spiel = "Spit out compiler pass-related CSV"

drive :: Args -> IO ()
drive Args{input} = do
  contents <- T.unpack <$> T.readFile input
  let ls = map consolidate $ NE.groupBy ((==) `on` fst)
                           $ map parseDumpTimingsLine
                           $ lines contents
  putStrLn "pass,time-in-milliseconds"
  for_ ls $ \(pass, time) -> do
    putStr $ show pass ++ ","
    printf "%.3f" time
    putStrLn ""

data Pass
  = Parse
  | RenameTypecheck
  | Desugar
  | Simplify
  | CoreTidy
  | CorePrep
  | CodeGen
  deriving (Eq, Ord, Read, Show)

consolidate :: NonEmpty (a, Double) -> (a, Double)
consolidate l =
  let pass  = fst $ NE.head l
      total = sum $ fmap snd l
  in (pass, total)

parsePass :: String -> Pass
parsePass "Parser"                       = Parse
parsePass "Renamer/typechecker"          = RenameTypecheck
parsePass "Desugar"                      = Desugar
parsePass "Simplifier"                   = Simplify
parsePass "Specialise"                   = Simplify
parsePass "Float out"                    = Simplify
parsePass "Called arity analysis"        = Simplify
parsePass "Demand analysis"              = Simplify
parsePass "Worker Wrapper binds"         = Simplify
parsePass "Exitification transformation" = Simplify
parsePass "Common sub-expression"        = Simplify
parsePass "Float inwards"                = Simplify
parsePass "Liberate case"                = Simplify
parsePass "SpecConstr"                   = Simplify
parsePass "CoreTidy"                     = CoreTidy
parsePass "CorePrep"                     = CorePrep
parsePass "CodeGen"                      = CodeGen
parsePass s                              = error $ "Unrecognized pass: " ++ show s

parseDumpTimingsLine :: String -> (Pass, Double)
parseDumpTimingsLine line =
  let pass = parsePass $ trim $ takeWhile (\c -> c `notElem` "([") line
      time = case lastMay (words line) of
               Nothing -> error $ "parse error on line: " ++ line
               Just t  -> read $ drop (length ("time=" :: String)) t
  in (pass, time)
