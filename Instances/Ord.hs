{-# LANGUAGE ConstraintKinds #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DefaultSignatures #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE EmptyCase #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE InstanceSigs #-}
{-# LANGUAGE PartialTypeSignatures #-}
{-# LANGUAGE PolyKinds #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE UndecidableInstances #-}
{-# OPTIONS_GHC -Wno-orphans #-}
{-# OPTIONS_GHC -Wno-partial-type-signatures #-}

module Instances.Ord () where

import Instances.Eq ()
import Instances.Generics ()
import VerifiedClasses.Ord

deriving anyclass instance POrd ()
deriving anyclass instance SOrd ()
deriving anyclass instance VOrd ()
deriving anyclass instance GOrd ()

deriving anyclass instance (POrd a, POrd b) => POrd (a, b)
deriving anyclass instance (SOrd a, SOrd b) => SOrd (a, b)
deriving anyclass instance (VOrd a, VOrd b) => VOrd (a, b)
deriving anyclass instance                _ => GOrd (a, b)
