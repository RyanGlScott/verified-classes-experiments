{-# LANGUAGE ConstraintKinds #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DefaultSignatures #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE InstanceSigs #-}
{-# LANGUAGE PartialTypeSignatures #-}
{-# LANGUAGE PolyKinds #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE UndecidableInstances #-}
{-# OPTIONS_GHC -Wno-orphans #-}
{-# OPTIONS_GHC -Wno-partial-type-signatures #-}

module Instances.Monoid () where

import Instances.Generics ()
import Instances.Semigroup ()
import VerifiedClasses.Monoid

deriving anyclass instance PMonoid ()
deriving anyclass instance SMonoid ()
deriving anyclass instance VMonoid ()
deriving anyclass instance GMonoid ()

deriving anyclass instance (PMonoid a, PMonoid b) => PMonoid (a, b)
deriving anyclass instance (SMonoid a, SMonoid b) => SMonoid (a, b)
deriving anyclass instance (VMonoid a, VMonoid b) => VMonoid (a, b)
deriving anyclass instance                      _ => GMonoid (a, b)
