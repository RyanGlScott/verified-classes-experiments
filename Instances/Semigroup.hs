{-# LANGUAGE ConstraintKinds #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DefaultSignatures #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE EmptyCase #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE InstanceSigs #-}
{-# LANGUAGE PartialTypeSignatures #-}
{-# LANGUAGE PolyKinds #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE UndecidableInstances #-}
{-# OPTIONS_GHC -Wno-orphans #-}
{-# OPTIONS_GHC -Wno-partial-type-signatures #-}

module Instances.Semigroup () where

import Instances.Generics ()
import VerifiedClasses.Semigroup

deriving anyclass instance PSemigroup ()
deriving anyclass instance SSemigroup ()
deriving anyclass instance VSemigroup ()
deriving anyclass instance GSemigroup ()

deriving anyclass instance (PSemigroup a, PSemigroup b) => PSemigroup (a, b)
deriving anyclass instance (SSemigroup a, SSemigroup b) => SSemigroup (a, b)
deriving anyclass instance (VSemigroup a, VSemigroup b) => VSemigroup (a, b)
deriving anyclass instance                            _ => GSemigroup (a, b)
