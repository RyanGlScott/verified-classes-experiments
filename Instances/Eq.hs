{-# LANGUAGE ConstraintKinds #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DefaultSignatures #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE InstanceSigs #-}
{-# LANGUAGE PartialTypeSignatures #-}
{-# LANGUAGE PolyKinds #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE UndecidableInstances #-}
{-# OPTIONS_GHC -Wno-orphans #-}
{-# OPTIONS_GHC -Wno-partial-type-signatures #-}

module Instances.Eq () where

import Instances.Generics ()
import VerifiedClasses.Eq

deriving anyclass instance PEq ()
deriving anyclass instance SEq ()
deriving anyclass instance VEq ()
deriving anyclass instance GEq ()

deriving anyclass instance (PEq a, PEq b) => PEq (a, b)
deriving anyclass instance (SEq a, SEq b) => SEq (a, b)
deriving anyclass instance (VEq a, VEq b) => VEq (a, b)
deriving anyclass instance              _ => GEq (a, b)
