{-# LANGUAGE CPP #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DeriveTraversable #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE InstanceSigs #-}
{-# LANGUAGE PartialTypeSignatures #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TypeInType #-}
{-# LANGUAGE UndecidableInstances #-}
{-# OPTIONS_GHC -Wno-partial-type-signatures #-}
module Product where

import Data.Singletons.TH (singletons)
import GHC.Generics
import Instances ()

import VerifiedClasses.Generics
import VerifiedClasses.TH

#ifdef USING_AbelianSemigroup
import VerifiedClasses.AbelianSemigroup
#endif

#ifdef USING_Applicative
import VerifiedClasses.Applicative
#endif

#ifdef USING_Eq
import VerifiedClasses.Eq
#endif

#ifdef USING_Ord
import VerifiedClasses.Ord
#endif

#ifdef USING_Functor
import VerifiedClasses.Functor
#endif

#ifdef USING_Monoid
import VerifiedClasses.Monoid
#endif

#ifdef USING_Semigroup
import VerifiedClasses.Semigroup
#endif

#ifdef USING_Traversable
import VerifiedClasses.Traversable
#endif

$(singletons [d|
  data ProductEx a = MkProductEx () a ((), a)
    -- deriving Show
  |])

-----
-- Verified instances
-----

deriving instance _ => Generic (ProductEx a)
deriving instance _ => Generic1 ProductEx
$(verifyGeneric0And1 ''ProductEx)

#ifdef USING_AbelianSemigroup
deriving instance _ => VAbelianSemigroup (ProductEx a)
#endif

#ifdef USING_Applicative
instance Applicative ProductEx where
  pure  = genericPure
  (<*>) = genericAp
deriving instance _ => PApplicative ProductEx
deriving instance _ => SApplicative ProductEx
deriving instance _ => VApplicative ProductEx
deriving instance _ => GApplicative ProductEx
#endif

#ifdef USING_Eq
instance Eq a => Eq (ProductEx a) where
  (==) = genericEq
deriving instance _ => PEq (ProductEx a)
deriving instance _ => SEq (ProductEx a)
deriving instance _ => VEq (ProductEx a)
deriving instance _ => GEq (ProductEx a)
#endif

#ifdef USING_Functor
instance Functor ProductEx where
  fmap = genericFmap
deriving instance _ => PFunctor ProductEx
deriving instance _ => SFunctor ProductEx
deriving instance _ => VFunctor ProductEx
deriving instance _ => GFunctor ProductEx
#endif

#ifdef USING_Monoid
instance Monoid a => Monoid (ProductEx a) where
  mempty = genericMempty
deriving instance _ => PMonoid (ProductEx a)
deriving instance _ => SMonoid (ProductEx a)
deriving instance _ => VMonoid (ProductEx a)
deriving instance _ => GMonoid (ProductEx a)
#endif

#ifdef USING_Ord
instance Ord a => Ord (ProductEx a) where
  (<=) = genericLeq
deriving instance _ => POrd (ProductEx a)
deriving instance _ => SOrd (ProductEx a)
deriving instance _ => VOrd (ProductEx a)
deriving instance _ => GOrd (ProductEx a)
#endif

#ifdef USING_Semigroup
instance Semigroup a => Semigroup (ProductEx a) where
  (<>) = genericAppend
deriving instance _ => PSemigroup (ProductEx a)
deriving instance _ => SSemigroup (ProductEx a)
deriving instance _ => VSemigroup (ProductEx a)
deriving instance _ => GSemigroup (ProductEx a)
#endif

#ifdef USING_Traversable
instance Traversable ProductEx where
  traverse = genericTraverse
deriving instance _ => PTraversable ProductEx
deriving instance _ => STraversable ProductEx
deriving instance _ => VTraversable ProductEx
deriving instance _ => GTraversable ProductEx
#endif

-----
-- Miscellaneous instances
-----

deriving instance _ => Foldable ProductEx
