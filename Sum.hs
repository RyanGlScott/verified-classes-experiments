{-# LANGUAGE CPP #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DeriveTraversable #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE InstanceSigs #-}
{-# LANGUAGE PartialTypeSignatures #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TypeInType #-}
{-# LANGUAGE UndecidableInstances #-}
{-# OPTIONS_GHC -Wno-partial-type-signatures #-}
module Sum where

import Data.Singletons.TH (singletons)
import GHC.Generics
import Instances ()

import VerifiedClasses.Generics
import VerifiedClasses.TH

#ifdef USING_Eq
import VerifiedClasses.Eq
#endif

#ifdef USING_Ord
import VerifiedClasses.Ord
#endif

#ifdef USING_Functor
import VerifiedClasses.Functor
#endif

#ifdef USING_Traversable
import VerifiedClasses.Traversable
#endif

$(singletons [d|
  data SumEx a = MkSumEx1
               | MkSumEx2 ()
               | MkSumEx3 a
               | MkSumEx4 ((), a)
    -- deriving Show
  |])

-----
-- Verified instances
-----

deriving instance _ => Generic (SumEx a)
deriving instance _ => Generic1 SumEx
$(verifyGeneric0And1 ''SumEx)

#ifdef USING_Eq
instance Eq a => Eq (SumEx a) where
  (==) = genericEq
deriving instance _ => PEq (SumEx a)
deriving instance _ => SEq (SumEx a)
deriving instance _ => VEq (SumEx a)
deriving instance _ => GEq (SumEx a)
#endif

#ifdef USING_Functor
instance Functor SumEx where
  fmap = genericFmap
deriving instance _ => PFunctor SumEx
deriving instance _ => SFunctor SumEx
deriving instance _ => VFunctor SumEx
deriving instance _ => GFunctor SumEx
#endif

#ifdef USING_Ord
instance Ord a => Ord (SumEx a) where
  (<=) = genericLeq
deriving instance _ => POrd (SumEx a)
deriving instance _ => SOrd (SumEx a)
deriving instance _ => VOrd (SumEx a)
deriving instance _ => GOrd (SumEx a)
#endif

#ifdef USING_Traversable
instance Traversable SumEx where
  traverse = genericTraverse
deriving instance _ => PTraversable SumEx
deriving instance _ => STraversable SumEx
deriving instance _ => VTraversable SumEx
deriving instance _ => GTraversable SumEx
#endif

-----
-- Miscellaneous instances
-----

deriving instance _ => Foldable SumEx
