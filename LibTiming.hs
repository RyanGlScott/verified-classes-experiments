#!/usr/bin/env stack
-- stack --resolver lts-13.7 script --optimize
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE OverloadedStrings #-}
{-# OPTIONS_GHC -Wall #-}
module Main (main) where

import Control.Monad.Extra
import Data.Foldable
import Data.List
import qualified Data.Text as T
import qualified Data.Text.IO as T
import Options.Applicative
import Safe
import System.Directory
import System.FilePath
import System.Process
import Text.Printf

data Args = Args
  { buildDir :: FilePath
  , output   :: FilePath
  } deriving (Eq, Ord, Read, Show)

argParser :: Parser Args
argParser = Args
  <$> strOption
      (  long "build-dir"
      <> short 'b'
      <> metavar "DIR"
      <> value ".stack-work"
      <> help "Where to look for .dump-timing files" )
  <*> strOption
      (  long "output"
      <> short 'o'
      <> metavar "FILE"
      <> value "lib-timing.csv"
      <> help "Where to write the output" )

main :: IO ()
main = execParser opts >>= drive
  where
    opts = info (argParser <**> helper)
      ( fullDesc
     <> progDesc spiel
     <> header spiel )

    spiel = "Run compilation timing information on verified-classes"

drive :: Args -> IO ()
drive args@Args{output} = do
  T.writeFile output "opt,filename,time-in-seconds\n"
  traverse_ (\opt -> measure opt args) ["-O0","-O1","-O2"]

measure :: String -- -O0, -O1, or -O2
        -> Args -> IO ()
measure opt Args{buildDir,output} = do
  whenM (doesDirectoryExist buildDir) $ removeDirectoryRecursive buildDir
  callProcess "stack" [ "build"
                      , "--stack-yaml=stack"++opt++".yaml"
                      , "verified-classes"
                      ]
  files <- getRecursiveContents buildDir
  let files' = filter (\file -> takeExtension file == ".dump-timings"
                            && (takeBaseName file `elem` classes)) files
  for_ (sort files') $ \file -> do
    T.appendFile output $ T.pack $ opt ++ ","
    T.appendFile output $ T.pack $ takeBaseName file ++ ","
    contents <- T.unpack <$> T.readFile file
    let time = parseDumpTimingsFile $ lines contents
    T.appendFile output $ T.pack $ printf "%.3f" (time * 1e-3)
    T.appendFile output "\n"

classes :: [String]
classes = [ "AbelianSemigroup"
          , "Alternative"
          , "Applicative"
          , "Eq"
          , "Functor"
          , "Monad"
          , "MonadPlus"
          , "MonadZip"
          , "Monoid"
          , "Ord"
          , "Semigroup"
          , "Traversable"
          ]

parseDumpTimingsFile :: [String] -> Double
parseDumpTimingsFile = sum . map parseDumpTimingsLine

parseDumpTimingsLine :: String -> Double
parseDumpTimingsLine line =
  case lastMay (words line) of
    Nothing -> error $ "parse error on line: " ++ line
    Just t  -> read $ drop (length ("time=" :: String)) t

getRecursiveContents :: FilePath -> IO [FilePath]
getRecursiveContents topdir = do
    names <- getDirectoryContents topdir
    let properNames = filter (`notElem` [".", ".."]) names
    paths <- forM properNames $ \name -> do
        let path = topdir </> name
        isDirectory <- doesDirectoryExist path
        if isDirectory then getRecursiveContents path else return [path]
    return (concat paths)
