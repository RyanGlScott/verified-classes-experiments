#!/usr/bin/env stack
-- stack --resolver lts-13.7 script --optimize
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE OverloadedStrings #-}
{-# OPTIONS_GHC -Wall #-}
module Main (main) where

import Control.Monad.Extra
import Data.Foldable
import Data.List
import Data.List.Split (splitOn)
import Data.Maybe
import qualified Data.Text as T
import qualified Data.Text.IO as T
import Options.Applicative
import Safe
import System.Directory
import System.FilePath
import System.IO.Silently
import System.Process
import Text.Printf

data Args = Args
  { buildDir   :: FilePath
  , mbExamples :: Maybe [String]
  , output     :: FilePath
  } deriving (Eq, Ord, Read, Show)

argParser :: Parser Args
argParser = Args
  <$> strOption
      (  long "build-dir"
      <> short 'b'
      <> metavar "DIR"
      <> value ".stack-work"
      <> help "Where to look for .dump-timing files" )
  <*> (optional . csListOption)
      (  long "examples"
      <> short 'e'
      <> metavar "{Product,Sum}"
      <> help "Which examples to test" )
  <*> strOption
      (  long "output"
      <> short 'o'
      <> metavar "FILE"
      <> value "sum-product.csv"
      <> help "Where to write the output" )

-- | Comma-separated lists of arguments.
csListOption :: Mod OptionFields String -> Parser [String]
csListOption flags = splitOn "," <$> strOption flags

main :: IO ()
main = execParser opts >>= drive
  where
    opts = info (argParser <**> helper)
      ( fullDesc
     <> progDesc spiel
     <> header spiel )

    spiel = "Run compilation timing information on Product/Sum examples"

data ProductOrSum = Product | Sum
  deriving (Eq, Ord, Read, Show)

drive :: Args -> IO ()
drive args@Args{output} = do
  T.writeFile output "opt,example,classes,time-in-seconds\n"
  traverse_ (\opt -> measure opt args) ["-O0","-O1","-O2"]

measure :: String -- -O0, -O1, or -O2
        -> Args -> IO ()
measure opt Args{buildDir, mbExamples, output} = do
  whenM (doesDirectoryExist buildDir) $ removeDirectoryRecursive buildDir
  let examples = fromMaybe ["Product","Sum"] mbExamples

      enumify :: String -> ProductOrSum
      enumify "Product" = Product
      enumify "Sum"     = Sum
      enumify s         = error $ s ++ " is not Product or Sum"

      examples' = map enumify examples
  for_ examples' $ \example -> do
    let exampleStr = show example
        exampleClasses = classes example
    for_ (inits exampleClasses) $ \exClasses -> do
      let classCPPs = concatMap (" -DUSING_"++) exClasses
          stackYaml = "--stack-yaml=stack" ++ opt ++ ".yaml"
      silence $ callProcess "stack" [ "clean"
                                    , stackYaml
                                    , "verified-classes-experiments"
                                    ]
      silence $ callProcess "stack" [ "build"
                                    , stackYaml
                                    , "--flag", "verified-classes-experiments:Use" ++ exampleStr
                                    , "--ghc-options", classCPPs
                                    ]
      files <- getRecursiveContents buildDir
      let [file] = filter (\file' -> takeExtension file' == ".dump-timings"
                                  && takeBaseName  file' == exampleStr) files
      T.appendFile output $ T.pack $ opt ++ "," ++ show example ++ ","
      let descr = fromMaybe "Base" $ lastMay exClasses
      T.appendFile output $ T.pack $ descr ++ ","
      contents <- T.unpack <$> T.readFile file
      let time = parseDumpTimingsFile $ lines contents
      T.appendFile output $ T.pack $ printf "%.3f" (time * 1e-3)
      T.appendFile output "\n"

parseDumpTimingsFile :: [String] -> Double
parseDumpTimingsFile = sum . map parseDumpTimingsLine

parseDumpTimingsLine :: String -> Double
parseDumpTimingsLine line =
  case lastMay (words line) of
    Nothing -> error $ "parse error on line: " ++ line
    Just t  -> read $ drop (length ("time=" :: String)) t

getRecursiveContents :: FilePath -> IO [FilePath]
getRecursiveContents topdir = do
    names <- getDirectoryContents topdir
    let properNames = filter (`notElem` [".", ".."]) names
    paths <- forM properNames $ \name -> do
        let path = topdir </> name
        isDirectory <- doesDirectoryExist path
        if isDirectory then getRecursiveContents path else return [path]
    return (concat paths)

classes :: ProductOrSum -> [String]
classes Product = productClasses
classes Sum     = sumClasses

productClasses, sumClasses :: [String]
productClasses = [ "Eq"
                 , "Ord"
                 , "Semigroup"
                 , "AbelianSemigroup"
                 , "Monoid"
                 , "Functor"
                 , "Applicative"
                 , "Traversable"
                 ]
sumClasses     = [ "Eq"
                 , "Ord"
                 , "Functor"
                 , "Traversable"
                 ]
